<?php
	/**
	 * Created by PhpStorm.
	 * User: Jee
	 * Date: 5/7/2018
	 * Time: 1:58 PM
	 */

	namespace Model;


	trait SettableJediFactorTrait
	{
		private $jediFactor;
		/**
		 * @return mixed
		 */
		public function getJediFactor()
		{
			return $this->jediFactor;
		}

		public function setJediFactor($jediFactor) {
			$this->jediFactor = $jediFactor;
		}
	}
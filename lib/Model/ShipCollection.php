<?php
	/**
	 * Created by PhpStorm.
	 * User: Jee
	 * Date: 5/6/2018
	 * Time: 2:48 PM
	 */

	namespace Model;


	use Traversable;


	/**
	 * Class ShipCollection
	 *
	 * Why did we do this?
	 * Sometimes it might be useful to add helpful methods to an array but you can't do that.
	 * You CAN add methods to an object and then make it act like an array or be used like an array.
	 *
	 * ** I can imagine doing something like this to work with an legacy system that uses arrays!
	 *
	 * @package Model
	 */
	class ShipCollection implements \ArrayAccess, \IteratorAggregate, \Countable
	{
		/**
		 * @var AbstractShip[]
		 */
		private $ships;

		public function __construct(array $ships) {
			$this->ships = $ships;
		}

		public function offsetExists($offset)
		{
			return array_key_exists($offset, $this->ships);
		}

		public function offsetGet($offset)
		{
			return $this->ships[$offset];
		}

		public function offsetSet($offset, $value)
		{
			$this->ships[$offset] = $value;
		}

		public function offsetUnset($offset)
		{
			unset($this->ships[$offset]);
		}

		public function getIterator()
		{
			return new \ArrayIterator($this->ships);
		}

		public function removeAllBrokenShips() {
			foreach ($this->ships as $key => $ship) {
				if (!$ship->isFunctional()) {
					unset($this->ships[$key]);
				}
			}
		}

		/**
		 * @return int
		 */
		public function count()
		{
			$count = 0;
			foreach ($this->ships as $key => $ship) {
				$count++;
			}
			return $count;
		}


	}
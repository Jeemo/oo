<?php
	/**
	 * Created by PhpStorm.
	 * User: Jee
	 * Date: 5/4/2018
	 * Time: 7:07 PM
	 */

	namespace Model;

	abstract class AbstractShip
	{
		private $id;
		private $name;
		private $weaponPower = 0;
		private $strength = 0;

		abstract public function getJediFactor();
    abstract public function getType();
    abstract public function isFunctional();

		public function __construct($name)
		{
			$this->name = $name;
		}

		/**
		 * @return string
		 */
		public function getName() {
			return $this->name;
		}

		/**
		 * @param string $name
		 */
		public function setName($name)
		{
			$this->name = $name;
		}

		/**
		 * @return int
		 */
		public function getWeaponPower()
		{
			return $this->weaponPower;
		}

		/**
		 * @param int $weaponPower
		 */
		public function setWeaponPower($weaponPower)
		{
			$this->weaponPower = $weaponPower;
		}

		/**
		 * @return int
		 */
		public function getStrength()
		{
			return $this->strength;
		}

		/**
		 * @param $strength
		 * @throws \Exception
		 */
		public function setStrength($strength)
		{
			if (!is_numeric($strength)) {
				throw new \Exception('Invalid strength value: '.$strength);
			}
			$this->strength = $strength;
		}

		public function sayHello() {
			echo 'HELLO!';
		}

		public function getNameAndSpecs($useShortFormat = false) {
			if ($useShortFormat) {
				return sprintf(
					'%s: %s/%s/%s',
					$this->name,
					$this->weaponPower,
					$this->getJediFactor(),
					$this->strength
				);
			} else {
				return sprintf(
					'%s: w:%s, j:%s, s:%s',
					$this->name,
					$this->weaponPower,
					$this->getJediFactor(),
					$this->strength
				);
			}
		}

		public function doesGivenShipHaveMoreStrength($givenShip) {
			return $givenShip->strength > $this->strength;
		}

		/**
		 * @return integer
		 */
		public function getId()
		{
			return $this->id;
		}

		/**
		 * @param integer $id
		 */
		public function setId($id)
		{
			$this->id = $id;
		}

		public function __toString() {
			return $this->getName();
		}
	}
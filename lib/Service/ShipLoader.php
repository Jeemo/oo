<?php
	/**
	 * Created by PhpStorm.
	 * User: Jee
	 * Date: 5/2/2018
	 * Time: 6:27 PM
	 */

	namespace Service;

	use Model\BountyHunterShip;
	use Model\RebelShip;
	use Model\Ship;
	use Model\ShipCollection;

	/**
	 * Class ShipLoader
	 *
	 * Service Classes do work
	 * Their properties do two things.
	 *
	 * 1. hold options on class behavior (no example of this yet.)
	 * 2. hold other tools like a PDO object
	 */
	class ShipLoader
	{
		/**
		 * @var ShipStorageInterface
		 */
		private $shipStorage;

		public function __construct(ShipStorageInterface $shipStorage) {
			$this->shipStorage = $shipStorage;
		}

		/**
		 * @return array|ShipCollection
		 * @throws \Exception
		 */
		public function getShips()
		{
			try {
				$shipsData = $this->shipStorage->fetchAllShipsData();
			} catch (\PDOException $e) {
				trigger_error('Database Exception! '.$e->getMessage());
				// If all else fails, just return an empty array
				return [];
			}

			$ships = array();
			foreach ($shipsData as $shipData) {
				$ships[] = $this->createShipFromData($shipData);
			}

			$ships[] = new BountyHunterShip('Slave I');

			return new ShipCollection($ships);
		}

		/**
		 * @param $id
		 * @return RebelShip|Ship
		 * @throws \Exception
		 */
		public function findOneById($id) {
			$shipArray = $this->shipStorage->fetchSingleShipData($id);
			return $this->createShipFromData($shipArray);
		}

		/**
		 * @param array $shipData
		 * @return RebelShip|Ship
		 * @throws \Exception
		 */
		private function createShipFromData(array $shipData) {
			if ($shipData['team'] == 'rebel') {
				$ship = new RebelShip($shipData['name']);
			} else {
				$ship = new Ship($shipData['name']);
				$ship->setJediFactor($shipData['jedi_factor']);
			}
			$ship->setId($shipData['id']);
			$ship->setWeaponPower($shipData['weapon_power']);
			$ship->setStrength($shipData['strength']);

			return $ship;
		}

	}
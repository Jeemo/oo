<?php

	namespace Service;

	/**
	 * Class Container
	 *
	 * This is a Dependency Injection Container class.
	 * It is a special type of class and you ALWAYS have just one!
	 *
	 * Its only job is to create service objects such as PDO, ShipLoader and BattleManager.
	 * If you do OOP right, ALL service objects are created HERE.
	 * Model objects like Ship and BattleResult are created wherever and whenever you need them.
	 */
	class Container {

		/**
		 * @var array
		 */
		private $configuration;

		private $pdo;

		private $shipLoader;

		private $shipStorage;

		private $battleManager;

		public function __construct(array $configuration) {
		  $this->configuration = $configuration;
		}

		/**
		 * @return \PDO
		 */
		public function getPDO() {
			if ($this->pdo === null) {
				$this->pdo = new \PDO(
					$this->configuration['db_dsn'],
					$this->configuration['db_user'],
					$this->configuration['db_pass']
				);
				$this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		}
			return $this->pdo;
		}

		/**
		 * @return ShipLoader
		 */
		public function getShipLoader() {
			if ($this->shipLoader === null) {
				$this->shipLoader = new ShipLoader($this->getShipStorage());
			}
			return $this->shipLoader;
		}

		/**
		 * @return ShipStorageInterface
		 */
		public function getShipStorage() {
			if ($this->shipStorage === null) {
				$this->shipStorage = new PdoShipStorage($this->getPDO());
				//$this->shipStorage = new JsonFileShipStorage(__DIR__.'/../../resources/ships.json');

				// Use 'composition': put the PdoShipStorage inside the LoggableShipStorage.
				$this->shipStorage = new LoggableShipStorage($this->shipStorage);
			}
			return $this->shipStorage;
		}

		/**
		 * @return mixed
		 */
		public function getBattleManager()
		{
			if ($this->battleManager === null) {
				$this->battleManager = new BattleManager();
			}
			return $this->battleManager;
		}


	}
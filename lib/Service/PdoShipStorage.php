<?php
	/**
	 * Created by PhpStorm.
	 * User: Jee
	 * Date: 5/4/2018
	 * Time: 8:07 PM
	 */

	namespace Service;

	use \PDO;

	class PdoShipStorage implements ShipStorageInterface
	{
		private $pdo;

		public function __construct(PDO $pdo) {
			$this->pdo = $pdo;
		}

		public function fetchAllShipsData() {
			$pdo = $this->pdo;
			$statement = $pdo->prepare('SELECT * FROM ship');
			$statement->execute();
			$shipsArray = $statement->fetchAll(PDO::FETCH_ASSOC);

			return $shipsArray;
		}

		public function fetchSingleShipData($id) {
			$pdo = $this->pdo;
			$statement = $pdo->prepare('SELECT * FROM ship WHERE id = :id');
			$statement->execute(array('id' => $id));
			$shipArray = $statement->fetch(PDO::FETCH_ASSOC);

			if (!$shipArray) {
				return null;
			}
			return $shipArray;
		}
	}
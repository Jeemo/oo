<?php
	/**
	 * Created by PhpStorm.
	 * User: Jee
	 * Date: 5/10/2018
	 * Time: 1:05 PM
	 */

	namespace Service;

	/**
	 * Class LoggableShipStorage
	 * @package Service
	 *
	 * Why is Composition Cool?
	 *
	 * Wrapping one object inside of another like this is called composition. You see, when you want to change
	 * the behavior of an existing class, the first thing we always think of is 'Oh, just extend that class and
	 * override some methods'.
	 *
	 * But composition is another option, and it does have some subtle advantages. If we had extended PDOShipStorage
	 * and then later wanted to change back to our JsonFileShipStorage, then all of a sudden we would need to
	 * change our LoggableShipStorage to extend JsonFileShipStorage. But with composition, our wrapper class can work
	 * with any ShipStorageInterface. We could change just one line to go back to loading files from JSON and not lose our logging.
	 *
	 * This isn't always a ground-breaking difference, but this is what people mean when they talk about "composition over inheritance".
	 *
	 */
	class LoggableShipStorage implements ShipStorageInterface
	{
		private $shipStorage;

		public function __construct(ShipStorageInterface $shipStorage) {
			$this->shipStorage = $shipStorage;
		}

		/**
		 * @return mixed
		 */
		public function fetchAllShipsData()
		{
			$ships = $this->shipStorage->fetchAllShipsData();
			$this->log(sprintf('Just fetched %s ships', count($ships)));
			return $ships;
		}

		/**
		 * @param $id
		 * @return mixed
		 */
		public function fetchSingleShipData($id)
		{
			$ship = $this->shipStorage->fetchSingleShipData($id);

			// Do whatever LoggableShipStorage needs to do.

			return $ship;
		}

		private function log($message) {
			//TODO: actually log this somewhere instead of just printing to prove it works.

			echo $message;

		}
	}
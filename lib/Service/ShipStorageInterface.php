<?php
	/**
	 * Created by PhpStorm.
	 * User: Jee
	 * Date: 5/5/2018
	 * Time: 12:58 PM
	 */

	namespace Service;

	interface ShipStorageInterface
	{
		/**
		 * @return array
		 * * Returns an array of ship arrays, each with the following keys:
		 * id
		 * name
		 * weaponPower
		 * defense
		 *
		 */
		public function fetchAllShipsData();

		/**
		 * Returns the single ship array for this id (see fetchAllShipsData)
		 *
		 * @param integer $id
		 * @return array
		 */
		public function fetchSingleShipData($id);
	}
<?php
	/**
	 * Created by PhpStorm.
	 * User: Jee
	 * Date: 5/7/2018
	 * Time: 1:46 PM
	 */

	namespace Model;


	class BountyHunterShip extends AbstractShip
	{
		use SettableJediFactorTrait;
		/**
		 * @return mixed
		 */
		public function getType()
		{
			return 'Bounty Hunter';
		}

		/**
		 * @return mixed
		 */
		public function isFunctional()
		{
			return true;
		}
	}
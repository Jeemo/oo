<?php


	namespace Model;

	/**
	 * Class Ship
	 *
	 * Model Classes are used to describe the details of an object
	 *
	 * Their properties are only used to hold data pertaining to that object.
	 *
	 */
	class Ship extends AbstractShip
	{
	  use SettableJediFactorTrait;

		private $underRepair;

		public function __construct($name)
		{
			parent::__construct($name);

			$this->underRepair = mt_rand(1, 100) < 30;
		}

		public function isFunctional()
		{
			return !$this->underRepair;
		}

		public function getType() {
			return 'Empire';
		}

	}

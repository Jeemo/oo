<?php
	/**
	 * Created by PhpStorm.
	 * User: Jee
	 * Date: 5/3/2018
	 * Time: 12:51 PM
	 */

	namespace Model;

	class BattleResult implements \ArrayAccess
	{
		private $usedJediPowers;
		private $winningShip;
		private $losingShip;

		public function __construct(bool $usedJediPowers, AbstractShip $winningShip = null, AbstractShip $losingShip = null)
		{
			$this->usedJediPowers = $usedJediPowers;
			$this->winningShip = $winningShip;
			$this->losingShip = $losingShip;
		}

		/**
		 * @return boolean
		 */
		public function wereJediPowersUsed()
		{
			return $this->usedJediPowers;
		}

		/**
		 * @return AbstractShip|null
		 */
		public function getWinningShip()
		{
			return $this->winningShip;
		}

		/**
		 * @return AbstractShip|null
		 */
		public function getLosingShip()
		{
			return $this->losingShip;
		}

		public function isThereAWinner() {
			return $this->getWinningShip() !== null;
		}

		// The following four functions are required by \ArrayAccess
		// Don't use these without a REALLY good reason, but DO know that they exist.
		// And they allow treating an object just like an array.
		// ie battle.php L89
		public function offsetExists($offset)
		{
			return property_exists($this, $offset);
		}

		public function offsetGet($offset)
		{
			return $this->$offset;
		}

		public function offsetSet($offset, $value)
		{
			$this->$offset = $value;
		}

		public function offsetUnset($offset)
		{
			unset($this->$offset);
		}

	}
Object Oriented Ship Battling, Ahem Programming
===============================================

This repository started as the Initial screencast code, script and blueprints for a
secret rebel base for the first episode of [Object Oriented Series](https://knpuniversity.com/screencast/oo)
from KnpUniversity.

I paid for a subscription to the AMAZING KnpUniversity.com 
and am completing all four of their OO programming track episodes.
 This repo contains my edited code.

Setup
-----

To get this code working, open your favorite terminal application
and start the built-in web server:

```bash
cd /path/to/the/project
php -S localhost:8000
```

This command will appear to "hang" - but that's perfect! You're
now running a temporary PHP web server (press ctrl+c to stop it
when you're done later).

Pull up the new site by going to:

    http://localhost:8000


<?php
	/**
	 * Created by PhpStorm.
	 * User: Jee
	 * Date: 5/4/2018
	 * Time: 7:56 PM
	 */

	namespace Model;

	class BrokenShip extends AbstractShip
	{

	  public function getJediFactor() {
	  	return 0;
		}

	  public function getType() {
			return 'Broken';
		}

		public function isFunctional() {
	  	return false;
		}
	}